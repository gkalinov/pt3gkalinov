package es.almata.gkalinov.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import es.almata.gkalinov.Calculadora;

class TestCalculadora {

	//@Test
	@ParameterizedTest
	@DisplayName("An + Bn = Cn")
	@CsvSource({"12,4,16",
			   "22,1,23",
			   "33,22,55",
			   "21,3,24",
			   "39,16,55"})
	void addParamArray(int a, int b, int result) {
		Calculadora c = new Calculadora();
		assertEquals(result, c.add(a, b));
	}
	@ParameterizedTest
	@DisplayName("An - Bn = Cn")
	@CsvSource({"12,4,8",
			   "22,1,21",
			   "33,22,11",
			   "21,3,18",
			   "39,16,23"})
	void subParamArray(int a, int b, int result) {
		Calculadora c = new Calculadora();
		assertEquals(result, c.sub(a, b));
	}
	@ParameterizedTest
	@DisplayName("An / Bn = Cn")
	@CsvSource({"12,4,3",
			   "22,1,22",
			   "33,3,11",
			   "21,3,7",
			   "40,5,8"})
	void divParamArray(int a, int b, int result) {
		Calculadora c = new Calculadora();
		assertEquals(result, c.div(a, b));
	}
	@ParameterizedTest
	@DisplayName("An * Bn = Cn")
	@CsvSource({"1,1,1",
			   "22,1,22",
			   "33,3,99",
			   "5,3,15",
			   "40,5,200"})
	void prodParamArray(int a, int b, int result) {
		Calculadora c = new Calculadora();
		assertEquals(result, c.prod(a, b));
	}
	
	
	@BeforeEach
	public void setUp() {
		System.out.println("s'execute a l'inici de cada test");
	}
	@AfterEach
	public void after() {
		System.out.println("s'execute al final de cada test");
	}
	@AfterAll
	public static void afterAll() {
		System.out.println("s'execute despres de tot");
	}
	@BeforeAll
	public static void before() {
		System.out.println("s'execute abans de tot");
	}
}
