package es.almata.gkalinov;

public class Persona {
	String nom;
	String cognoms;
	int edat;
	
	
	
	public Persona(String nom, String cognoms, int edat) {
		super();
		this.nom = nom;
		this.cognoms = cognoms;
		this.edat = edat;
	}
	
	
	@Override
	public String toString() {
		return "Persona [nom=" + nom + ", cognoms=" + cognoms + ", edat=" + edat + "]";
	}


	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCognoms() {
		return cognoms;
	}
	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		this.edat = edat;
	}
	
	
}
